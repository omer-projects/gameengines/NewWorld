# NewWorldPlugin

NewWorldPlugin [option] path
	path					 - Open the.nwe with Visual Studio Code
	--help                   - Show this help
	--install-extension      - Install the extension
	--uninstall-extension    - Uninstall the extension
	--generate-projects path - Generate Projects
	--build path             - Build the applications