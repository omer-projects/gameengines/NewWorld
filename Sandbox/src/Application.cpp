#include <NewWorld.h>
#include <NewWorld/EntryPoint.h>

#include "Settings.h"
#include "Tests/Main.h"

namespace Sandbox
{
	class Application : NewWorld::Application
	{
		NW_CLASS(Sandbox, Application)

	public:
		Application()
		{
			
		}

		void Initialize() override
		{
			Tests::TestsRoot();
		}
	};
}

ENTRYPOINT_APPLICATION(Sandbox::Application);