# DevOps

## Git

Branchs Names:
master - The Last Release
	release-<release> - Example: release-1
	development - The last Stage / System that developed
		dev-<release>.<stage>-"stage-name" - the current stage, Example: dev-0.1-DevOps
			dev-<release>.<stage>.<sub stage>-"sub-stage-name" - the current sub stage,  Example: dev-0.1.1-VSCodePlugin

## TODO

Priorities:

P0: Urgent (Red)      - Must todo now / or Bug.
P1: Substage (Orange) - In develop in this substage.
    Test (Gray)       - Developed in this substage and need testing.
P3: Task (White)      - A task for the future.
P4: Option (Gray)     - A option task for the future.
P2: Project (Green)   - A root category of project.

Tags:
[arg1, arg2] - Requires that other features / stages will developed first.
                arg - feature / stage