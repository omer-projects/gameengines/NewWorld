# Links

## Git
https://gitlab.com/Omer-Projects/GameEngines/NewWorld

## LICENSE
https://opensource.org/licenses/MIT

## Azure
* Project	 : https://dev.azure.com/OmerPrielNewWorld
* Marketplace: https://marketplace.visualstudio.com/manage/publishers/newworld

## Logo
https://www.canva.com/design/DAEm07pxUUk/oyicj5ii7maokDOFNL1bdQ/edit

## Dependencies
* Tools
	Premake	 : https://github.com/premake/premake-core
* Extensions
	Todo List: https://marketplace.visualstudio.com/items?itemName=saber2pr.todolist

* Libraries
	GLM		: https://github.com/g-truc/glm
	GLFW	: https://github.com/glfw/glfw
	ImGui	: https://github.com/ocornut/imgui