#pragma once

#include <GLM/glm.hpp>

namespace NewWorld::Math
{
	using Vector2 = glm::vec2;
}
