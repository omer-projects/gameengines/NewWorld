#pragma once

#include <GLM/glm.hpp>

namespace NewWorld::Math
{
	using Vector4 = glm::vec4;
}