#pragma once

#include <GLM/glm.hpp>

namespace NewWorld::Math
{
	using Matrix3 = glm::mat3;
}