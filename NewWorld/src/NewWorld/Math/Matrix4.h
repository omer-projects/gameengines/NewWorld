#pragma once

#include <GLM/glm.hpp>

namespace NewWorld::Math
{
	using Matrix4 = glm::mat4;
}