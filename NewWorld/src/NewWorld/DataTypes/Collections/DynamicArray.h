#pragma once

#include <vector>

namespace NewWorld::DataTypes::Collections
{
	template <typename T>
	using DynamicArray = std::vector<T>;
}