#pragma once
#include "NewWorld/Minimal.h"

#include "NewWorld/Debug/LogLevel.h"

constexpr char NW_CONST_ENGINE_LOGGER_1_NAME[] = "Engine/Graphics";
constexpr NewWorld::Debug::LogLevel NW_CONST_ENGINE_LOGGER_1_DISPLAY_LEVEL = NewWorld::Debug::LogLevel::Debug;
constexpr char NW_CONST_ENGINE_LOGGER_0_NAME[] = "Engine/Core";
constexpr NewWorld::Debug::LogLevel NW_CONST_ENGINE_LOGGER_0_DISPLAY_LEVEL = NewWorld::Debug::LogLevel::Debug;